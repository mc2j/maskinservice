package main

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gorilla/mux"

	handler "gitlab.com/mc2j/maskinservice/internal/app/delivery/http"
)

func main() {
	args := os.Args[1:]
	port := "8000"
	if len(args) == 1 {
		port = args[0]
	}

	if err := removeContents("./runtime/lock", ".lock"); err != nil {
		log.Fatalln("cant purge lock dir")
	}
	if err := removeContents("./runtime/yaml", ".yaml"); err != nil {
		log.Fatalln("cant purge yaml dir")
	}
	if err := removeContents("./runtime/export", ".sql"); err != nil {
		log.Fatalln("cant purge export dir")
	}

	r := mux.NewRouter()
	r.HandleFunc("/", handler.IndexHandler).Methods(http.MethodGet)
	r.HandleFunc("/api/tables", handler.TablesHandler).Methods(http.MethodPost)
	r.HandleFunc("/api/fields", handler.FieldsHandler).Methods(http.MethodPost)
	r.HandleFunc("/api/mask", handler.MaskHandler).Methods(http.MethodPost)

	r.HandleFunc("/api/fk", handler.FkHandler).Methods(http.MethodPost)
	r.HandleFunc("/api/fk/table", handler.TableFkHandler).Methods(http.MethodGet)

	r.HandleFunc("/api/yaml", handler.GetConfigHandler).Methods(http.MethodGet)
	r.HandleFunc("/api/file", handler.GetFileHandler).Methods(http.MethodGet)

	log.Println("service started on :" + port)

	if err := http.ListenAndServe(":"+port, r); err != nil {
		log.Fatalln(err)
	}
}

func removeContents(dir string, postfix string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		if !strings.HasSuffix(name, postfix) {
			continue
		}

		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
