package main

import (
	"bytes"
	"context"
	"log"
	"os"

	"github.com/google/uuid"
	"gopkg.in/yaml.v3"

	"gitlab.com/mc2j/maskinservice/internal/app/service/masking"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		log.Fatalln("not enough args")
	}

	f, err := os.Open(args[0])
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	b := new(bytes.Buffer)
	if _, err := b.ReadFrom(f); err != nil {
		log.Fatalln(err)
	}

	content := types.MaskConfigFileContent{}
	if err := yaml.Unmarshal(b.Bytes(), &content); err != nil {
		log.Fatalln(err)
	}

	s := masking.New(content.Config)
	guid := uuid.New().String()
	if err := s.Masking(context.Background(), guid, content.Masks); err != nil {
		log.Fatalln(err)
	}

	log.Println("file " + guid + ".sql created")
}
