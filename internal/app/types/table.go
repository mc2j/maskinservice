package types

type RawTable struct {
	SchemaName  string  `db:"table_schema"`
	Name        string  `db:"table_name"`
	Description *string `db:"description"`
}

type DbFk struct {
	ConstraintName string `db:"constraint_name"`
	Schema         string `db:"table_schema"`
	Table          string `db:"table_name"`
	Column         string `db:"column_name"`
	ForeignSchema  string `db:"foreign_table_schema"`
	ForeignTable   string `db:"foreign_table_name"`
	ForeignColumn  string `db:"foreign_column_name"`
}

type Fk struct {
	Key    bool
	Name   string
	Schema string
	Table  string
	Column string
}

type Table struct {
	Name        string
	Description string
}

type Schema struct {
	Name       string
	TableCount int64
	Tables     []Table
}

type TableColumns struct {
	Name        string
	TrimmedName string // Имя таблицы, которое пойдет в html аттрибут ID
	Columns     []Column
}

type Column struct {
	Name        string  `db:"name"`
	Description *string `db:"description"`
	Fks         []Fk
}

type FieldMask struct {
	Column string
	Mask   string
}

type TableMask struct {
	Name     string
	SubWhere string
	Fields   []FieldMask
}
