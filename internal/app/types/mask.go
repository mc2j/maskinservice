package types

type MaskConfigFileContent struct {
	Config Config
	Masks  map[string]*TableMask
}
