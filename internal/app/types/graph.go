package types

type NodeTable struct {
	Id    int64  `json:"id"`
	Table string `json:"table"`
}

type LinkTable struct {
	ParentId     int64  `json:"parentId"`
	ChildId      int64  `json:"childId"`
	ForeignKey   string `json:"foreignKey"`
	ParentColumn string `json:"parentColumn"`
	ChildColumn  string `json:"childColumn"`
}

type LinkTableWithValues struct {
	LinkTable
	Values map[string]struct{}
}

type TreeInfo struct {
	Elements []NodeTable      `json:"elements"`
	Links    []LinkTable      `json:"links"`
	TableMap map[string]int64 `json:"tableMap"`
}

type TreeNode map[int64]string
