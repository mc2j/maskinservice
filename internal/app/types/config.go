package types

type Config struct {
	DbConn    string `json:"db_conn"`
	EmailHost string `json:"email_host"`
}
