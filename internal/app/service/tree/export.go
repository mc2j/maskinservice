package tree

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mc2j/maskinservice/internal/app/provider/mask"
	"gitlab.com/mc2j/maskinservice/internal/app/service/value"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

type Service struct {
	provider *mask.Provider
}

func New(conf types.Config) *Service {
	provider := mask.New(conf)

	return &Service{
		provider: provider,
	}
}

func (s *Service) Export(
	ctx context.Context,
	content types.MaskConfigFileContent,
	guid string,
	data types.TreeInfo,
	filter string,
) error {
	lock, err := os.Create("./runtime/lock/" + guid + ".lock")
	if err != nil {
		return err
	}
	if err := lock.Close(); err != nil {
		return err
	}

	// сортируем ноды по id
	tree := types.TreeNode{}
	for _, node := range data.Elements {
		tree[node.Id] = node.Table
	}

	f, err := os.Create("./runtime/export/" + guid + ".sql")
	if err != nil {
		return err
	}

	defer func() { _ = f.Close() }()

	// Отсортируем все в мапу map[имя таблицы][имя колонки] = маска
	maskingInfo := make(map[string]map[string]string)
	for _, tableMask := range content.Masks {
		if _, ok := maskingInfo[tableMask.Name]; !ok {
			maskingInfo[tableMask.Name] = make(map[string]string)
		}
		for _, columnMask := range tableMask.Fields {
			maskingInfo[tableMask.Name][columnMask.Column] = columnMask.Mask
		}
	}

	db, err := sqlx.Connect("postgres", content.Config.DbConn)
	if err != nil {
		return err
	}

	wheres := make(map[string][]string)
	if err := fillWhere(ctx, db, 1, filter, f, tree, data.Links, wheres); err != nil {
		return err
	}

	if err := s.export(ctx, db, f, data.TableMap, maskingInfo, wheres); err != nil {
		return err
	}

	if err := os.Remove("./runtime/lock/" + guid + ".lock"); err != nil {
		log.Println("cant remove lock file on " + guid + " uuid")
	}

	return nil
}

func (s *Service) export(
	ctx context.Context,
	db *sqlx.DB,
	f io.Writer,
	tableMap map[string]int64,
	maskingInfo map[string]map[string]string,
	wheres map[string][]string,
) error {
	reverseTableMap := make(map[int64][]string)
	for table, i := range tableMap {
		if _, ok := reverseTableMap[i]; !ok {
			reverseTableMap[i] = make([]string, 0)
		}
		reverseTableMap[i] = append(reverseTableMap[i], table)
	}

	keys := make([]int, len(reverseTableMap))
	i := 0
	for k := range reverseTableMap {
		keys[i] = int(k)
		i++
	}
	sort.Ints(keys)

	for _, k := range keys {
		for _, table := range reverseTableMap[int64(k)] {
			where := ""
			if v, ok := wheres[table]; ok {
				if len(v) != 0 {
					where = "and ((" + strings.Join(v, ") or (") + "))"
				}
			}

			if err := s.exportTable(ctx, db, table, where, maskingInfo, f); err != nil {
				return err
			}
		}
	}

	return nil
}

func (s *Service) exportTable(
	ctx context.Context,
	db *sqlx.DB,
	table string,
	where string,
	maskingInfo map[string]map[string]string,
	f io.Writer,
) error {
	selectQ := fmt.Sprintf(`select * from %s where 1 = 1 %s`, table, where)
	rows, err := db.QueryContext(ctx, selectQ)
	if err != nil {
		return err
	}

	defer func() { _ = rows.Close() }()

	cts, err := rows.ColumnTypes()
	if err != nil {
		return err
	}
	colTypes := make(map[string]string, len(cts))
	cols := make([]string, 0)
	for _, ct := range cts {
		colTypes[ct.Name()] = ct.DatabaseTypeName()
		cols = append(cols, ct.Name())
	}

	q := ""
	i := 0
	for rows.Next() {
		exportData := make(map[string]interface{})
		clsP := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range clsP {
			columnPointers[i] = &clsP[i]
		}
		if err := rows.Scan(columnPointers...); err != nil {
			return err
		}
		for i, colName := range cols {
			exportData[colName] = clsP[i]
		}

		keys := make([]string, 0, len(exportData))
		for k := range exportData {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		cols := make([]string, 0, len(exportData))
		vals := make([]string, 0, len(exportData))
		for _, key := range keys {
			cols = append(cols, key)
			vals = append(
				vals,
				value.WrapValue(
					s.provider.Mask(exportData[key], maskingInfo[table][key], i),
					colTypes[key],
				),
			)
		}

		if i == 0 {
			q = fmt.Sprintf("insert into %s (\"%s\") values\r\n(%s)", table, strings.Join(cols, `","`), strings.Join(vals, ","))
		} else {
			q += fmt.Sprintf(",\r\n(%s)", strings.Join(vals, ","))
		}
		i++
	}

	if q != "" {
		q = "-- Данные " + table + "\r\n" + q + ";\r\n\r\n"
		if _, err := f.Write([]byte(q)); err != nil {
			return err
		}
		log.Printf("Данные по таблице %s с фильтром %s записаны", table, where)
	}

	return nil
}

func fillWhere(
	ctx context.Context,
	db *sqlx.DB,
	nodeId int64,
	filter string,
	f io.Writer,
	tree types.TreeNode,
	links []types.LinkTable,
	wheres map[string][]string,
) error {
	// найдем все линки к текущей таблице
	currentNodeLinks := make([]types.LinkTableWithValues, 0)
	for _, link := range links {
		if link.ParentId == nodeId {
			linkWithValues := types.LinkTableWithValues{
				LinkTable: link,
				Values:    make(map[string]struct{}),
			}
			currentNodeLinks = append(currentNodeLinks, linkWithValues)
		}
	}

	// берем первую ноду, применяем фильтр
	where := ""
	if filter != "" {
		where = fmt.Sprintf("and (%s)", filter)
	}

	if _, ok := wheres[tree[nodeId]]; !ok {
		wheres[tree[nodeId]] = make([]string, 0)
	}
	if filter != "" {
		wheres[tree[nodeId]] = append(wheres[tree[nodeId]], filter)
	}

	selectQ := fmt.Sprintf(`select * from %s where 1 = 1 %s`, tree[nodeId], where)
	rows, err := db.QueryContext(ctx, selectQ)
	if err != nil {
		return err
	}

	defer func() { _ = rows.Close() }()

	cts, err := rows.ColumnTypes()
	if err != nil {
		return err
	}
	colTypes := make(map[string]string, len(cts))
	cols := make([]string, 0)
	for _, ct := range cts {
		colTypes[ct.Name()] = ct.DatabaseTypeName()
		cols = append(cols, ct.Name())
	}

	for rows.Next() {
		exportData := make(map[string]interface{})
		clsP := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range clsP {
			columnPointers[i] = &clsP[i]
		}
		if err := rows.Scan(columnPointers...); err != nil {
			return err
		}
		for i, colName := range cols {
			exportData[colName] = clsP[i]
		}

		keys := make([]string, 0, len(exportData))
		for k := range exportData {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		cols := make([]string, 0, len(exportData))
		vals := make([]string, 0, len(exportData))
		for _, key := range keys {
			cols = append(cols, key)
			vals = append(
				vals,
				value.WrapValue(
					exportData[key],
					colTypes[key],
				),
			)

			// найдем ее значения для дальнейшего фильтра по foreign key
			for lwvI, linkWithValue := range currentNodeLinks {
				if linkWithValue.ParentColumn != key {
					continue
				}
				val := value.WrapValue(exportData[key], colTypes[key])
				currentNodeLinks[lwvI].Values[val] = struct{}{}
			}
		}
	}

	for _, l := range currentNodeLinks {
		if len(l.Values) == 0 {
			continue
		}

		ins := make([]string, len(l.Values))
		i := 0
		for k := range l.Values {
			ins[i] = k
			i++
		}

		filterWhereIn := fmt.Sprintf(`"%s" in (%s)`, l.ChildColumn, strings.Join(ins, ","))
		if err := fillWhere(ctx, db, l.ChildId, filterWhereIn, f, tree, links, wheres); err != nil {
			return err
		}
	}

	return nil
}
