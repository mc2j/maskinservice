package column

import (
	"context"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func GetColumnsByTables(ctx context.Context, db *sqlx.DB, tableNames []string) ([]types.TableColumns, error) {
	tableColumns := make([]types.TableColumns, 0, len(tableNames))
	for _, t := range tableNames {
		tableInfo := strings.Split(t, ".")
		tableColumn := types.TableColumns{
			Name:        t,
			TrimmedName: strings.ReplaceAll(t, ".", "__"),
			Columns:     make([]types.Column, 0),
		}

		if err := db.SelectContext(ctx, &(tableColumn.Columns), fmt.Sprintf(`
			SELECT c.column_name name, col_description('%s'::regClass, c.ordinal_position) description
				FROM information_schema.columns c
				WHERE table_schema = $1
				AND table_name = $2
		`, t), tableInfo[0], tableInfo[1]); err != nil {
			return nil, err
		}

		tableColumns = append(tableColumns, tableColumn)
	}

	return tableColumns, nil
}

func GetColumns(ctx context.Context, db *sqlx.DB, tableFullName string) ([]types.Column, error) {
	tableParts := strings.Split(tableFullName, ".")

	var cols []types.Column
	if err := db.SelectContext(ctx, &cols, fmt.Sprintf(`
			SELECT c.column_name name, col_description('%s'::regClass, c.ordinal_position) description
				FROM information_schema.columns c
				WHERE table_schema = $1
				AND table_name = $2
		`, tableFullName), tableParts[0], tableParts[1]); err != nil {
		return nil, err
	}

	return cols, nil
}
