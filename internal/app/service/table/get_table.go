package table

import (
	"context"
	"strings"

	"github.com/go-openapi/swag"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"

	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func GetTables(ctx context.Context, db *sqlx.DB, excludeSchemasString string) (map[string]*types.Schema, error) {
	dbExSchemas := []string{"information_schema", "pg_catalog"}
	excludedSchemas := excludeSchemasString
	if excludedSchemas != "" {
		for _, es := range strings.Split(excludedSchemas, ",") {
			dbExSchemas = append(dbExSchemas, strings.Trim(es, " "))
		}
	}

	var dest []types.RawTable
	if err := db.SelectContext(ctx, &dest, `
		select
		   t.table_schema,
		   t.table_name,
		   obj_description((t.table_schema||'.'||quote_ident("table_name"))::regclass) description
		from information_schema.tables t
		where not(table_schema = any($1)) and t.table_type='BASE TABLE'
		group by t.table_schema, t.table_name
		order by table_schema, "table_name"
	`, pq.Array(dbExSchemas)); err != nil {
		return nil, err
	}

	tables := make(map[string]*types.Schema)
	for _, t := range dest {
		if e, ok := tables[t.SchemaName]; !ok {
			tables[t.SchemaName] = &types.Schema{
				Name:       t.SchemaName,
				TableCount: 0,
				Tables:     make([]types.Table, 0),
			}
		} else {
			e.TableCount++
			e.Tables = append(e.Tables, types.Table{
				Name:        t.Name,
				Description: swag.StringValue(t.Description),
			})
		}
	}

	return tables, nil
}
