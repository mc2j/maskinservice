package value

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

func WrapValue(value interface{}, t string) string {
	if value == nil {
		return "null"
	}
	switch strings.ToLower(t) {
	case "varchar", "text", "date", "timestamptz":
		return fmt.Sprintf(`'%v'`, value)
	case "jsonb":
		return fmt.Sprintf(`'%s'`, value)
	case "int8", "bool", "int4", "int2":
		return fmt.Sprintf(`%v`, value)
	case "_int8":
		b := value.([]byte)
		return fmt.Sprintf("'%s'", string(b))
	case "numeric":
		// число приходит массивом байт
		b := value.([]byte)
		byteToInt, _ := strconv.Atoi(string(b))
		return fmt.Sprintf("%d", byteToInt)
	case "":
		// todo: определить кастомный тип
		b := value.([]byte)
		return fmt.Sprintf("'%s'", string(b))
	default:
		log.Printf(`не найден обработчик типа "%v", применен стандартный, получено: %v`, t, value)
		return fmt.Sprintf(`%v`, value)
	}
}
