package fk

import (
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func GetTableForeignKeys(ctx context.Context, db *sqlx.DB, schema, table string) (map[string][]types.Fk, error) {
	// todo сделать множественные foreign
	// todo сделать агрегацию isFk здесь, чтобы не перекладывать
	q := `SELECT
		tc.table_schema,
		tc.table_name,
		kcu.column_name,
		ccu.table_schema AS foreign_table_schema,
		ccu.table_name AS foreign_table_name,
		ccu.column_name AS foreign_column_name
	FROM
		information_schema.table_constraints AS tc
			JOIN information_schema.key_column_usage AS kcu
				 ON tc.constraint_name = kcu.constraint_name
					 AND tc.table_schema = kcu.table_schema
			JOIN information_schema.constraint_column_usage AS ccu
				 ON ccu.constraint_name = tc.constraint_name
					 AND ccu.table_schema = tc.table_schema and ccu.table_name = $2 and ccu.table_schema = $1
	WHERE tc.constraint_type = 'FOREIGN KEY'
	group by tc.table_schema, tc.table_name, kcu.column_name, ccu.table_schema, ccu.table_name, ccu.column_name

	union all
	
	SELECT
		tc.table_schema,
		tc.table_name,
		kcu.column_name,
		ccu.table_schema AS foreign_table_schema,
		ccu.table_name AS foreign_table_name,
		ccu.column_name AS foreign_column_name
	FROM
		information_schema.table_constraints AS tc
			JOIN information_schema.key_column_usage AS kcu
				 ON tc.constraint_name = kcu.constraint_name
					 AND tc.table_schema = kcu.table_schema
			JOIN information_schema.constraint_column_usage AS ccu
				 ON ccu.constraint_name = tc.constraint_name
					 AND ccu.table_schema = tc.table_schema
	WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name = $2 and tc.table_schema = $1
	group by tc.table_schema, tc.table_name, kcu.column_name, ccu.table_schema, ccu.table_name, ccu.column_name`

	var fks []types.DbFk
	if err := db.SelectContext(ctx, &fks, q, schema, table); err != nil {
		return nil, err
	}

	colFks := make(map[string][]types.Fk)
	for _, fk := range fks {
		isKey := fk.ForeignSchema == schema && fk.ForeignTable == table
		col := fk.Column
		if isKey {
			col = fk.ForeignColumn
		}

		sch := fk.Schema
		tab := fk.Table
		cl := fk.Column
		if !isKey {
			sch = fk.ForeignSchema
			tab = fk.ForeignTable
			cl = fk.ForeignColumn
		}

		if _, ok := colFks[col]; !ok {
			colFks[col] = make([]types.Fk, 0)
		}

		colFks[col] = append(colFks[col], types.Fk{
			Key:    isKey,
			Name:   fk.ConstraintName,
			Schema: sch,
			Table:  tab,
			Column: cl,
		})
	}

	return colFks, nil
}
