package masking

import (
	"context"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"

	"gitlab.com/mc2j/maskinservice/internal/app/provider/mask"
	"gitlab.com/mc2j/maskinservice/internal/app/service/value"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

type Service struct {
	conf     types.Config
	provider *mask.Provider
}

func New(conf types.Config) *Service {
	provider := mask.New(conf)

	return &Service{
		conf:     conf,
		provider: provider,
	}
}

func (s *Service) Masking(ctx context.Context, guid string, masks map[string]*types.TableMask) error {
	f, err := os.Create("./runtime/lock/" + guid + ".lock")
	// todo: wrap всех ошибок
	if err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}

	maskingInfo := make(map[string]map[string]string)
	tableSubQueries := make(map[string]string)

	// Отсортируем все в мапу map[имя таблицы][имя колонки] = маска
	for _, tableMask := range masks {
		if _, ok := maskingInfo[tableMask.Name]; !ok {
			maskingInfo[tableMask.Name] = make(map[string]string)
			tableSubQueries[tableMask.Name] = tableMask.SubWhere
		}
		for _, columnMask := range tableMask.Fields {
			maskingInfo[tableMask.Name][columnMask.Column] = columnMask.Mask
		}
	}

	db, err := sqlx.Connect("postgres", s.conf.DbConn)
	if err != nil {
		return err
	}

	f, err = os.Create("./runtime/export/" + guid + ".sql")
	if err != nil {
		return err
	}

	defer f.Close()

	for table, columns := range maskingInfo {
		log.Println("старт записи данных их таблицы " + table)

		cols := make([]string, 0, len(columns))
		for columnName := range columns {
			cols = append(cols, columnName)
		}

		query := fmt.Sprintf(`select "%s" from %s`, strings.Join(cols, `","`), table)
		if v, ok := tableSubQueries[table]; ok && v != "" {
			query += " where " + v
		}
		// todo: пагинатор
		rows, err := db.QueryContext(ctx, query)
		if err != nil {
			return err
		}

		defer rows.Close()

		cts, err := rows.ColumnTypes()
		if err != nil {
			return err
		}
		colTypes := make(map[string]string, len(cts))
		for _, ct := range cts {
			colTypes[ct.Name()] = ct.DatabaseTypeName()
		}

		isFirst := true
		q := ""
		i := 0
		for rows.Next() {
			data := make(map[string]interface{})
			clsP := make([]interface{}, len(cols))
			columnPointers := make([]interface{}, len(cols))
			for i, _ := range clsP {
				columnPointers[i] = &clsP[i]
			}
			if err := rows.Scan(columnPointers...); err != nil {
				return errors.Wrap(err, "scanning columns as pointers")
			}
			for i, colName := range cols {
				data[colName] = clsP[i]
			}

			keys := make([]string, 0, len(data))
			for k := range data {
				keys = append(keys, k)
			}
			sort.Strings(keys)

			cols := make([]string, 0, len(data))
			vals := make([]string, 0, len(data))
			for _, key := range keys {
				cols = append(cols, key)
				vals = append(
					vals,
					value.WrapValue(
						s.provider.Mask(data[key], maskingInfo[table][key], i),
						colTypes[key],
					),
				)
			}

			if isFirst {
				q = fmt.Sprintf("insert into %s (\"%s\") values\r\n(%s)", table, strings.Join(cols, `","`), strings.Join(vals, ","))
			} else {
				q += fmt.Sprintf(",\r\n(%s)", strings.Join(vals, ","))
			}
			isFirst = false
			i++
		}

		if q != "" {
			q = "-- Данные " + table + "\r\n" + q + ";\r\n\r\n"
			if _, err := f.Write([]byte(q)); err != nil {
				return err
			}
		}
		log.Println("данные по таблице " + table + " записаны")
	}

	if err := os.Remove("./runtime/lock/" + guid + ".lock"); err != nil {
		log.Println("cant remove lock file on " + guid + " uuid")
	}

	return nil
}

func (s *Service) ProviderTypes() map[string]mask.Type {
	return s.provider.Types()
}
