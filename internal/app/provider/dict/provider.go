package dict

import (
	"log"
	"math/rand"
)

type Provider struct {
	dicts map[string][]string
}

func New() *Provider {
	return &Provider{
		dicts: map[string][]string{
			"firstName":  firstNames,
			"lastName":   lastNames,
			"middleName": middleNames,
		},
	}
}

func (p *Provider) GetDict(dict string) []string {
	d, ok := p.dicts[dict]
	if !ok {
		return []string{}
	}
	return d
}

func (p *Provider) GetFromDict(dict string) string {
	d, ok := p.dicts[dict]
	if !ok || len(d) == 0 {
		log.Println("не удалось найти словарь \"" + dict + "\"")
		return ""
	}

	return d[rand.Intn(len(d))]
}
