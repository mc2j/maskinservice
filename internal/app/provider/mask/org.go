package mask

func (p *Provider) inn(_ interface{}, _ int) interface{} {
	return randString(digits, 10)
}

func (p *Provider) kpp(_ interface{}, _ int) interface{} {
	return randString(digits, 9)
}
