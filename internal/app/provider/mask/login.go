package mask

import (
	"strconv"
)

func (p *Provider) login(_ interface{}, counter int) interface{} {
	return "login" + strconv.Itoa(counter)
}
