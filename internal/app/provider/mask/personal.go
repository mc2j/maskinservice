package mask

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"strconv"
)

func (p *Provider) firstName(_ interface{}, _ int) interface{} {
	return p.dictProvider.GetFromDict("firstName")
}

func (p *Provider) lastName(_ interface{}, _ int) interface{} {
	return p.dictProvider.GetFromDict("lastName")
}

func (p *Provider) middleName(_ interface{}, _ int) interface{} {
	return p.dictProvider.GetFromDict("middleName")
}

func (p *Provider) fio(_ interface{}, _ int) interface{} {
	return fmt.Sprintf(
		"%s %s %s",
		p.dictProvider.GetFromDict("lastName"),
		p.dictProvider.GetFromDict("firstName"),
		p.dictProvider.GetFromDict("middleName"),
	)
}

func (p *Provider) email(_ interface{}, counter int) interface{} {
	return "email" + strconv.Itoa(counter) + "@" + p.conf.EmailHost
}

func (p *Provider) phone(_ interface{}, _ int) interface{} {
	return "8" + randString(digits, 10)
}

func (p *Provider) password(_ interface{}, _ int) interface{} {
	h := md5.New()
	_, err := io.WriteString(h, "123456")
	if err != nil {
		log.Println("не удалось захешировать пароль в провайдере маскирования")
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}
