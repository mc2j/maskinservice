package mask

import (
	"log"

	"gitlab.com/mc2j/maskinservice/internal/app/provider/dict"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

type Provider struct {
	conf         types.Config
	dictProvider *dict.Provider
}

func New(conf types.Config) *Provider {
	return &Provider{
		conf:         conf,
		dictProvider: dict.New(),
	}
}

func (p *Provider) Types() map[string]Type {
	return map[string]Type{
		"login": {
			Name: "Логин",
			Func: p.login,
		},
		"firstName": {
			Name: "Имя",
			Func: p.firstName,
		},
		"lastName": {
			Name: "Фамилия",
			Func: p.lastName,
		},
		"middleName": {
			Name: "Отчество",
			Func: p.middleName,
		},
		"fio": {
			Name: "ФИО",
			Func: p.fio,
		},
		"email": {
			Name: "Email",
			Func: p.email,
		},
		"phone": {
			Name: "Телефон",
			Func: p.phone,
		},
		"password": {
			Name: "Пароль",
			Func: p.password,
		},
		"inn": {
			Name: "ИНН",
			Func: p.inn,
		},
		"kpp": {
			Name: "КПП",
			Func: p.kpp,
		},
		"sequence": {
			Name: "Последовательность",
			Func: p.sequence,
		},
	}
}

func (p *Provider) Mask(value interface{}, mask string, i int) interface{} {
	if mask == "" {
		return value
	}

	provider, ok := p.Types()[mask]
	if !ok {
		log.Println("маска \"" + mask + "\" не найдена")
		return value
	}

	return provider.Func(value, i)
}

type Func func(value interface{}, counter int) interface{}

type Type struct {
	Name string
	Func Func
}
