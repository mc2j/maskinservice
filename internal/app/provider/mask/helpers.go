package mask

import "math/rand"

var digits = "0123456789"

func randString(alphabet string, length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = alphabet[rand.Intn(len(alphabet))]
	}
	return string(b)
}
