package http

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var fMap = template.FuncMap{
	"html": func(value interface{}) template.HTML {
		return template.HTML(fmt.Sprint(value))
	},
}

func TemplateResponse(w http.ResponseWriter, templateFile string, data interface{}) {
	headerTmpl, err := template.ParseFiles("./storage/header.html")
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	footerTmpl, err := template.ParseFiles("./storage/footer.html")
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := headerTmpl.Funcs(fMap).Execute(w, nil); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	RawTemplateResponse(w, templateFile, data)

	if err := footerTmpl.Funcs(fMap).Execute(w, nil); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func RawTemplateResponse(w http.ResponseWriter, templateFile string, data interface{}) {
	tmpl, err := template.ParseFiles(templateFile)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := tmpl.Funcs(fMap).Execute(w, data); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
