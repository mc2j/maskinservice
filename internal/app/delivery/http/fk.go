package http

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func FkHandler(w http.ResponseWriter, r *http.Request) {
	conf := types.Config{
		DbConn:    r.PostFormValue("db_addr"),
		EmailHost: r.PostFormValue("email_host"),
	}

	var errors []string
	if conf.DbConn == "" {
		errors = append(errors, "Укажите url подключения базы данных")
	}
	if conf.EmailHost == "" {
		errors = append(errors, "Укажите постфикс для email")
	}

	var exportEntityTableParts []string
	exportEntityTable := r.PostFormValue("entity_table")
	if exportEntityTable == "" {
		errors = append(errors, "Укажите таблицу, содержащую экспортируемую сущность")
	} else {
		exportEntityTableParts = strings.Split(exportEntityTable, ".")
		if len(exportEntityTableParts) < 2 {
			errors = append(errors, "Таблица сущности не содержит схему")
		}
	}

	bConf, err := json.Marshal(conf)
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	if len(errors) > 0 {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": errors,
		})
		return
	}

	TemplateResponse(w, "./storage/fk.html", map[string]interface{}{
		"table": exportEntityTable,
		"props": map[string]string{
			"db": conf.DbConn,
		},
		"config": string(bConf),
	})
}
