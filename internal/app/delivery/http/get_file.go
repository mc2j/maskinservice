package http

import (
	"bytes"
	"log"
	"net/http"
	"os"
)

func GetFileHandler(w http.ResponseWriter, r *http.Request) {
	var uuids []string
	var ok bool

	q := r.URL.Query()
	if uuids, ok = q["uuid"]; !ok {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{"Не передан параметр guid"},
		})
		return
	}

	// check lock file - if exists, then sql is forming and not accessible
	f, err := os.Open("./runtime/lock/" + uuids[0] + ".lock")
	if err == nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{"Файл готовится. Обновите страницу чуть позже. Если файл получить так и не удается, обратитесь к администратору."},
		})
		return
	}

	f, err = os.Open("./runtime/export/" + uuids[0] + ".sql")
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}
	defer f.Close()

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(f); err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	w.Header().Set("Content-Disposition", "attachment; filename="+uuids[0]+".sql")
	if _, err := w.Write(buf.Bytes()); err != nil {
		log.Println(err)
	}
}
