package http

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"gitlab.com/mc2j/maskinservice/internal/app/service/column"
	"gitlab.com/mc2j/maskinservice/internal/app/service/masking"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func FieldsHandler(w http.ResponseWriter, r *http.Request) {
	conf := types.Config{}
	postConf := r.PostFormValue("config")
	if err := json.Unmarshal([]byte(postConf), &conf); err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	db, err := sqlx.Connect("postgres", conf.DbConn)
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	graph := r.PostFormValue("graph")

	tableNames := make([]string, 0)
	for key := range r.PostForm {
		parts := strings.Split(key, ".")
		if parts[0] != "table" {
			continue
		}
		tableNames = append(tableNames, parts[1]+"."+parts[2])
	}

	tablesInfoWithColumns, err := column.GetColumnsByTables(r.Context(), db, tableNames)
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	s := masking.New(conf)

	TemplateResponse(w, "./storage/columns.html", map[string]interface{}{
		"tables": tablesInfoWithColumns,
		"config": postConf,
		"masks":  s.ProviderTypes(),
		"graph":  graph,
	})
}
