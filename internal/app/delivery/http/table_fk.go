package http

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mc2j/maskinservice/internal/app/service/column"
	"gitlab.com/mc2j/maskinservice/internal/app/service/fk"
)

func TableFkHandler(w http.ResponseWriter, r *http.Request) {
	table := r.URL.Query().Get("table")
	dbConn := r.URL.Query().Get("db")
	depth := r.URL.Query().Get("depth")
	i, err := strconv.Atoi(r.URL.Query().Get("i"))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if table == "" || dbConn == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	tableParts := strings.Split(table, ".")
	if len(tableParts) < 2 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	db, err := sqlx.Connect("postgres", dbConn)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	cols, err := column.GetColumns(r.Context(), db, table)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fks, err := fk.GetTableForeignKeys(r.Context(), db, tableParts[0], tableParts[1])
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for i, col := range cols {
		foreign, ok := fks[col.Name]
		if !ok {
			continue
		}

		cols[i].Fks = foreign
	}

	RawTemplateResponse(w, "./storage/table_fk.html", map[string]interface{}{
		"table": table,
		"cols":  cols,
		"index": i,
		"depth": depth,
	})
}
