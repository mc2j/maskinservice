package http

import (
	"bytes"
	"log"
	"net/http"
	"os"
)

func GetConfigHandler(w http.ResponseWriter, r *http.Request) {
	var uuids []string
	var ok bool

	q := r.URL.Query()
	if uuids, ok = q["uuid"]; !ok {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{"Не передан параметр guid"},
		})
		return
	}

	f, err := os.Open("./runtime/yaml/" + uuids[0] + ".yaml")
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}
	defer f.Close()

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(f); err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/yaml")
	if _, err := w.Write(buf.Bytes()); err != nil {
		log.Println(err)
	}
}
