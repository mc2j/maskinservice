package http

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"gopkg.in/yaml.v3"

	"gitlab.com/mc2j/maskinservice/internal/app/service/masking"
	"gitlab.com/mc2j/maskinservice/internal/app/service/tree"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func MaskHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	conf := types.Config{}
	postConf := r.PostFormValue("config")
	if err := json.Unmarshal([]byte(postConf), &conf); err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	masks := getMasksFromRequest(r)

	content := types.MaskConfigFileContent{
		Config: conf,
		Masks:  masks,
	}

	guid := uuid.New().String()
	b, err := yaml.Marshal(content)
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	f, err := os.Create("./runtime/yaml/" + guid + ".yaml")
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	defer func() {
		if err := f.Close(); err != nil {
			log.Println(err)
		}
	}()

	if _, err := f.Write(b); err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	TemplateResponse(w, "./storage/start_export.html", guid)

	go func() {
		graph := r.PostFormValue("graph")

		if graph == "" {
			service := masking.New(content.Config)

			if err := service.Masking(context.Background(), guid, content.Masks); err != nil {
				log.Println(err)
			}
		} else {
			data := types.TreeInfo{}
			if err := json.Unmarshal([]byte(graph), &data); err != nil {
				log.Println(err)
			}

			service := tree.New(content.Config)
			if err := service.Export(context.Background(), content, guid, data, r.PostFormValue("filter")); err != nil {
				log.Println(err)
			}
		}
	}()
}

func getMasksFromRequest(r *http.Request) map[string]*types.TableMask {
	masks := make(map[string]*types.TableMask)
	for key, vals := range r.PostForm {
		t := strings.Split(key, ".")
		if t[0] != "mask" {
			continue
		}

		subWhere := ""
		if sw := r.PostFormValue("filter." + t[1] + "." + t[2]); sw != "" {
			subWhere = sw
		}

		col := types.FieldMask{
			Column: t[3],
			Mask:   vals[0],
		}
		if val, ok := masks[t[1]+"."+t[2]]; !ok {
			masks[t[1]+"."+t[2]] = &types.TableMask{
				Name:     t[1] + "." + t[2],
				SubWhere: subWhere,
				Fields:   []types.FieldMask{col},
			}
		} else {
			val.Fields = append(val.Fields, col)
		}
	}

	return masks
}
