package http

import (
	"net/http"
)

func IndexHandler(w http.ResponseWriter, _ *http.Request) {
	TemplateResponse(w, "./storage/index.html", nil)
}
