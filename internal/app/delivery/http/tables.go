package http

import (
	"encoding/json"
	"net/http"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mc2j/maskinservice/internal/app/service/table"
	"gitlab.com/mc2j/maskinservice/internal/app/types"
)

func TablesHandler(w http.ResponseWriter, r *http.Request) {
	conf := types.Config{
		DbConn:    r.PostFormValue("db_addr"),
		EmailHost: r.PostFormValue("email_host"),
	}

	var errors []string
	if conf.DbConn == "" {
		errors = append(errors, "Укажите url подключения базы данных")
	}
	if conf.EmailHost == "" {
		errors = append(errors, "Укажите постфикс для email")
	}

	if len(errors) > 0 {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": errors,
		})
		return
	}

	bConf, err := json.Marshal(conf)
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	db, err := sqlx.Connect("postgres", conf.DbConn)
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	tables, err := table.GetTables(r.Context(), db, r.PostFormValue("excluded_schemas"))
	if err != nil {
		TemplateResponse(w, "./storage/errors.html", map[string]interface{}{
			"errors": []string{err.Error()},
		})
		return
	}

	TemplateResponse(w, "./storage/tables.html", map[string]interface{}{
		"tables": tables,
		"config": string(bConf),
	})
}
