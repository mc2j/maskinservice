module gitlab.com/mc2j/maskinservice

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.2.0
)

require (
	github.com/go-openapi/swag v0.22.3
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/josharian/intern v1.0.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
)
